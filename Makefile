PROJECT_DIR := gitlab_scripts
PROJECT_EXTRA_DIRS :=

# ------------------------------------------------------------------------------
# Python variables
PYTHON ?= python3
PIP ?= pip3
PYTESTFLAGS ?=
PYCOVFLAGS ?=

TWINE_REPOSITORY ?= pypi
TWINEFLAGS ?=

BLACKFLAGS ?=

# ------------------------------------------------------------------------------
# Generic variables
SPHINXBUILDFLAGS ?=

PRECOMMITFLAGS ?= --show-diff-on-failure

# ==============================================================================
# Generic
# ==============================================================================
.DEFAULT_GOAL:= all

.PHONY: all
all: check

.PHONY: help
help: ## Show this help
	@echo "Available targets:"
	@grep '^[a-z].*##' $(MAKEFILE_LIST) | sort | sed 's/\([a-z][a-z]*\)[^:]*:/\1:/' | awk -F ':.*?## ' 'NF==2 {printf "  %-26s%s\n", $$1, $$2}'

.PHONY: check test tests
check test tests: ## run unit-tests

.PHONY: coverage
coverage: ## runs coverage

.PHONY: lint
lint: ## runs linter

.PHONY: public
public:
	@mkdir -p public/reports public/pytest public/coverage

.PHONY: clean
clean: ## clean generated cache files

.PHONY: distclean
distclean: clean ## clean built files

.PHONY: html docs
html docs: ## build html documentation

.PHONY: dist package
dist package: ## build distribution package

.PHONY: precommit pre-commit pc
precommit pre-commit pc: ## run pre-commit on all files
	pre-commit run -a $(PRECOMMITFLAGS)

# ==============================================================================
# Python3
# ==============================================================================
.PHONY: pycheck pytest
check test tests: pycheck
pycheck pytest: public
	$(PYTHON) -m pytest -W ignore::DeprecationWarning \
	    --junitxml=public/reports/unit-tests.xml \
	    --html=public/pytest/index.html --self-contained-html \
	    $(PYTESTFLAGS) -- tests

.PHONY: pycov
coverage: pycov
pycov: public
	$(PYTHON) -m pytest -W ignore::DeprecationWarning \
	    --cov=$(PROJECT_DIR)  \
	    --cov-report term-missing \
	    --cov-report=html:"$(shell pwd)/public/coverage/" \
	    --cov-report=xml:"$(shell pwd)/public/reports/coverage.xml" \
	    --junitxml=public/reports/unit-tests.xml \
	    --html=public/pytest/index.html --self-contained-html \
	    $(PYTESTFLAGS) $(PYCOVFLAGS) -- tests
	@echo "Coverage generated here: file://$(shell pwd)/public/coverage/index.html"

.PHONY: black
black: ## python: format (with 'black') all files
	black -q $(BLACKFLAGS) -- \
	    $(PROJECT_DIR) $(PROJECT_EXTRA_DIRS) \
	    $(wildcard bin) $(wildcard tests) $(wildcard setup.py) $(wildcard docs/conf.py)

.PHONY: pylint
lint: pylint
pylint: public
	$(PYTHON) -m pylint $(PYLINTFLAGS) --recursive y \
	    $(PROJECT_DIR) $(PROJECT_EXTRA_DIRS) \
	    $(wildcard bin) $(wildcard tests) $(wildcard setup.py)

.PHONY: pyclean
clean: pyclean
pyclean:
	@find $(PROJECT_EXTRA_DIRS) $(PROJECT_DIR) $(wildcard bin) $(wildcard tests) -type f -name \*\.pyc -exec rm {} \;
	@find $(PROJECT_EXTRA_DIRS) $(PROJECT_DIR) $(wildcard bin) $(wildcard tests) -type f -name \*\.py,cover -exec rm {} \;
	@find $(PROJECT_EXTRA_DIRS) $(PROJECT_DIR) $(wildcard bin) $(wildcard tests) -type d -name __pycache__ -empty -delete
	@rm -f *.pyc *.py,cover
	@-rm -fr __pycache__

.PHONY: pydistclean
distclean: pydistclean
pydistclean: pyclean
	@rm -fr public/docs public/coverage public/pytest public/reports
	@-rm -fr *.egg-info dist .pytest_cache .coverage
	@-if test -d public ; then rmdir public; fi

# ------------------------------------------------------------------------------
# pypi packages
.PHONY: pydist wheel
dist: pycheck pydist
pydist wheel: pydistclean
	$(PYTHON) setup.py check -s
	$(PYTHON) setup.py sdist --dist-dir dist
	# https://github.com/pypa/wheel/issues/99
	$(PYTHON) -mpip wheel --require-virtualenv --disable-pip-version-check \
	    --no-deps --no-cache-dir --no-use-pep517 \
	    --wheel-dir dist -- dist/*

# Upload to repository
.PHONY: pydist-upload
pydist-upload: pydist
	$(PYTHON) -mtwine check dist/*
	$(PYTHON) -mtwine upload -r $(TWINE_REPOSITORY) $(TWINEFLAGS) dist/*

# ------------------------------------------------------------------------------
# sphinx documentation
.PHONY: sphinx pydoc pydoc-html
html docs: sphinx
sphinx pydoc pydoc-html: public
	if test -f docs/conf.py ; then \
	  PYTHONPATH=".:$$PYTHONPATH" sphinx-build -b html $(SPHINXBUILDFLAGS) docs ./public/docs/html ; \
	fi
	@echo "Documentation generated here: file://$(shell pwd)/public/docs/html/index.html"

# ==============================================================================
# Local targets
# ==============================================================================
