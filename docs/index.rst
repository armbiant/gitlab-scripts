gitlab_scripts
==============

Helper and convenience scripts to help with common GitLab actions.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   introduction
   project-badge
   runner

   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
