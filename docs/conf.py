# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import datetime
import os

import tomli

year = datetime.date.today().year

config = os.path.dirname(__file__) + "/../pyproject.toml"
with open(config, mode="rb") as fd:
    about = tomli.load(fd)["project"]

# ------------------------------------------------------------------------------
# Project information
author = about["authors"][0]["name"]
copyright = f"Copyright {year}, {author}"
project = about["name"]
release = about["version"]

# ------------------------------------------------------------------------------
# General configuration
# ------------------------------------------------------------------------------
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.napoleon",
    "sphinx.ext.intersphinx",
]

intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
    # "flask": ("https://flask.palletsprojects.com/en/2.2.x/", None),
    # "attrs": ("https://www.attrs.org/en/stable/", None),
}

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

source_suffix = ".rst"

# When enabled, emphasize placeholders in command line option directives
option_emphasise_placeholders = True

autodoc_typehints_format = "short"
autodoc_preserve_defaults = False

smart_quotes = False

# ------------------------------------------------------------------------------
# Options for HTML output
# ------------------------------------------------------------------------------
try:
    import sphinx_rtd_theme

    html_theme = "sphinx_rtd_theme"
except:
    html_theme = "alabaster"

html_static_path = ["static"]
html_css_files = ["custom.css"]
html_show_sourcelink = False
html_use_smartypants = False

# ------------------------------------------------------------------------------
# Misc
# ------------------------------------------------------------------------------
#  * Use 'ev' for envvar css formatting
#
rst_prolog = """
.. role:: ev
  :class: envvar
"""
rst_epilog = ""


def setup(app):
    pass
