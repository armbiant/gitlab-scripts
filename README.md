[![license](https://img.shields.io/badge/license-MIT-brightgreen)](https://spdx.org/licenses/MIT.html)
[![pipelines](https://gitlab.com/jlecomte/python/gitlab-scripts/badges/master/pipeline.svg)](https://gitlab.com/jlecomte/python/gitlab-scripts/pipelines)
[![coverage](https://gitlab.com/jlecomte/python/gitlab-scripts/badges/master/coverage.svg)](https://jlecomte.gitlab.io/python/gitlab_scripts/coverage/index.html)

# gitlab-scripts

Helper and convenience scripts to help with common GitLab actions.

## Installation from PyPI

You can install the latest version from PyPI package repository.

~~~bash
python3 -mpip install -U gitlab_scripts
~~~

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Locations

  * GitLab: [https://gitlab.com/jlecomte/python/gitlab-scripts](https://gitlab.com/jlecomte/python/gitlab-scripts)
  * PyPi: [https://pypi.org/project/gitlab_scripts](https://pypi.org/project/gitlab_scripts)
