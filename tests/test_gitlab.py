import responses
from pytest import raises

from gitlab_scripts.gitlab import connect, is_admin


def test_connect(config):
    with raises(SystemExit):
        connect(config)


def test_is_admin(gitlab):
    with responses.RequestsMock() as response:
        response.get(
            "https://example.com/api/v4/runners/all",
            json={},
            status=200,
        )
        assert is_admin(gitlab) is True

    with responses.RequestsMock() as response:
        response.get(
            "https://example.com/api/v4/runners/all",
            json={},
            status=403,
        )
        assert is_admin(gitlab) is False
