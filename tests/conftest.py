import responses
from pytest import fixture

from gitlab_scripts.gitlab import connect


@fixture(name="config")
def config_fixture():
    return {"gitlab": {"host": "https://example.com", "apikey": "secret"}}


@fixture(name="gitlab")
def gitlab_fixture(config):
    with responses.RequestsMock() as response:
        response.get(
            "https://example.com/api/v4/user",
            json={"id": 123},
            status=200,
        )
        return connect(config)
