import responses

from gitlab_scripts.runner.delete import action


def test_runner_delete(gitlab):
    args = {"query": "status == 'online'", "dry_run": True, "extend": False}
    with responses.RequestsMock() as response:
        response.get(
            "https://example.com/api/v4/runners/all",
            json=[
                {
                    "id": 233788,
                    "active": True,
                    "online": True,
                    "status": "online",
                },
                {
                    "id": 233789,
                    "active": True,
                    "online": True,
                    "status": "stale",
                },
            ],
            status=200,
        )
        action({}, args, gitlab=gitlab)
