from pytest import mark, raises

from gitlab_scripts.lexer import lex2python


@mark.parametrize(
    "expression, expected",
    # fmt: off
    [
        ("", ""),
        ("true", "true"),
        ("false", "false"),
        ("(true)", "( true )"),
    ],
    # fmt: on
)
def test_lexer(expression, expected):
    assert lex2python(expression) == expected


@mark.parametrize(
    "expression",
    # fmt: off
    [
        ("("),
        (")"),
        ("(}"),
        ("{]"),
        ("[)"),
    ],
    # fmt: on
)
def test_lexer_brackets(expression):
    with raises(SystemExit):
        lex2python(expression)
