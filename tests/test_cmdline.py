from pytest import mark, raises

from gitlab_scripts.__main__ import main


@mark.parametrize(
    "argv",
    # fmt: off
    [
        (""),
        ("project-badge duplicate src dst"),
        ("runner delete false"),
        ("runner list false"),
    ],
    # fmt: on
)
def test_cmdline(argv):
    argv = "--cfgfile /dev/null" + argv
    with raises(SystemExit):
        main(argv.split(" "))
